/* eslint-env node */
const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')

module.exports = {
	mode: 'production',
	context: path.resolve(__dirname, 'src'),
	entry: './index.tsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.[contenthash].js',
		assetModuleFilename: '[path][name][ext][query]',
		clean: true,
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/u,
				use: ['babel-loader'],
			},
			{
				test: /\.tsx?$/u,
				use: ['babel-loader', 'ts-loader'],
			},
			{
				test: /\.css$/u,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.woff2$/u,
				type: 'asset/resource',
			},
		],
	},
	resolve: {
		/* This is required to allow .ts and .tsx imports without an explicit extension. (TypeScript doesn't let you use explicit .ts or .tsx extensions in imports anyway.) */
		extensions: ['.tsx', '.ts', '.jsx', '.js'],
		alias: {
			react: 'preact/compat',
			'react-dom/test-utils': 'preact/test-utils',
			// Must be below test-utils, according to Preact docs.
			'react-dom': 'preact/compat',
		},
	},
	plugins: [
		new HtmlPlugin({
			template: 'index.ejs',
			meta: {
				viewport: 'width=device-width, initial-scale=1',
			},
			title: 'Zeb’s Basic Calculator (Preact version)',
			xhtml: true,
		}),
	],
}
