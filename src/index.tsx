// External dependencies
import { h, render } from 'preact'

// Project dependencies
import './assets/base-styles.css'
import App from './components/App'

render(<App />, document.body)
