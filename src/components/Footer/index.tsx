// External dependencies
import { h, JSX } from 'preact'

// Internal dependencies
import './style.css'

export default function Footer(): JSX.Element {
	return (
		<footer className="zeb-footer">
			<p>
				© 2018-2021{' '}
				<a href="https://zebulan.com" rel="external">
					Zebulan Stanphill
				</a>
				. Project released under{' '}
				<a
					rel="license external"
					href="https://www.gnu.org/licenses/gpl-3.0.html"
				>
					GPLv3+
				</a>
				.{' '}
				<a
					href="https://gitlab.com/ZebulanStanphill/zeb-basic-calculator-preact"
					rel="external"
				>
					Source code
				</a>
			</p>
		</footer>
	)
}
