// External dependencies
import useId from '@accessible/use-id'
import Decimal from 'decimal.js'
import { h, JSX } from 'preact'
import { useMemo, useState } from 'preact/hooks'
import toFormat from 'toformat'

// Project component dependencies
import Button from '../Button'

// Internal dependencies
import './style.css'

const Dec = toFormat(Decimal)

// This declaration is merged with the official one. It adds the toFormat method to the Decimal type, which at this point in the code has already been added to the Decimal prototype by the toFormat function call above.
// Obviously, this is a bit of a hacky way of doing things. But after hours of searching online, I was unable to find a better solution. It's kind of weird to be modifying classes dynamically anyway, so it's not surprising that it's difficult to make TypeScript happy when you do.
declare module 'decimal.js' {
	interface Decimal {
		toFormat(
			dp?: number,
			rm?: number,
			fmt?: {
				decimalSeparator?: string
				groupSeparator?: string
				groupSize?: number
				secondaryGroupSize?: number
				fractionGroupSeparator?: string
				fractionGroupSize?: number
			}
		): string
	}
}

enum BinaryOperator {
	Add,
	Divide,
	Multiply,
	Subtract,
}

const OPERATOR_SYMBOLS: Record<BinaryOperator, string> = {
	[BinaryOperator.Add]: '+',
	[BinaryOperator.Subtract]: '−',
	[BinaryOperator.Multiply]: '×',
	[BinaryOperator.Divide]: '∕',
}

enum CalculatorMode {
	Start,
	AfterDigit,
	AfterOperator,
}

// 0 at end since calculators traditionally place the 0 button after the other digits.
const DIGITS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0] as const
type Digit = typeof DIGITS[number]
export function isDigit(num: number): num is Digit {
	// https://stackoverflow.com/a/62943000
	return DIGITS.includes(num as Digit) ? true : false
}

function doBinaryOperation(n1: Decimal, n2: Decimal, operator: BinaryOperator) {
	switch (operator) {
		case BinaryOperator.Add:
			return n1.plus(n2)
		case BinaryOperator.Subtract:
			return n1.minus(n2)
		case BinaryOperator.Multiply:
			return n1.times(n2)
		case BinaryOperator.Divide:
			return n1.dividedBy(n2)
	}
}

export default function Calculator(): JSX.Element {
	const [currentInputString, setCurrentInputString] = useState('0')
	const [mode, setMode] = useState(CalculatorMode.Start)
	const [lastNumber, setLastNumber] = useState(new Dec('0'))
	const [
		lastBinaryOperator,
		setLastBinaryOperator,
	] = useState<BinaryOperator | null>(null)
	const outputId = useId(undefined, 'zbc-calculator__display__output-')

	// User-facing string shown in calculator display. Adds commas to large numbers.
	const display = useMemo(() => {
		if (mode === CalculatorMode.Start || mode === CalculatorMode.AfterDigit) {
			// Parsing "0." into a number will just give 0, so to make it clear to the user that their input was taken, append the locale's decimal separator to the end of the displayed number.
			if (currentInputString.endsWith('.')) {
				return new Dec(currentInputString).toFormat() + '.'
			} else {
				return new Dec(currentInputString).toFormat()
			}
		} else {
			// If the display is currently showing a binary operator, just show it as-is.
			return currentInputString
		}
	}, [currentInputString])

	function reset(): void {
		setLastNumber(new Dec('0'))
		setLastBinaryOperator(null)
		setMode(CalculatorMode.Start)
		setCurrentInputString('0')
	}

	function backspace(): void {
		if (mode === CalculatorMode.AfterDigit) {
			if (currentInputString.length > 1)
				setCurrentInputString(currentInputString.slice(0, -1))
			else setCurrentInputString('0')
		}
	}

	// When a digit button is pressed.
	function inputDigit(digit: Digit): void {
		if (mode === CalculatorMode.AfterDigit)
			setCurrentInputString(currentInputString.concat(String(digit)))
		else setCurrentInputString(String(digit)) // If at initial state or after operator.

		setMode(CalculatorMode.AfterDigit)
	}

	// When the decimal point button is pressed.
	function inputDecimalPoint(): void {
		if (
			mode === CalculatorMode.AfterDigit &&
			!currentInputString.includes('.') // Helpful: https://regex101.com
		) {
			setCurrentInputString(currentInputString.concat('.'))
		} else if (
			mode === CalculatorMode.Start ||
			mode === CalculatorMode.AfterOperator
		) {
			setCurrentInputString('0.')
		}

		setMode(CalculatorMode.AfterDigit)
	}

	function negate(): void {
		if (mode === CalculatorMode.Start || mode === CalculatorMode.AfterDigit) {
			setCurrentInputString(String(-currentInputString))
		}
	}

	function inputBinaryOperator(operator: BinaryOperator): void {
		if (mode === CalculatorMode.AfterDigit || mode === CalculatorMode.Start) {
			if (lastBinaryOperator !== null) {
				setLastNumber(
					doBinaryOperation(
						lastNumber,
						new Dec(currentInputString),
						lastBinaryOperator
					)
				)
			} else {
				setLastNumber(new Dec(currentInputString))
			}
			setLastBinaryOperator(operator)
			setCurrentInputString(OPERATOR_SYMBOLS[operator])
			setMode(CalculatorMode.AfterOperator)
		}
	}

	const inputAddOperator = () => inputBinaryOperator(BinaryOperator.Add)

	const inputSubtractOperator = () =>
		inputBinaryOperator(BinaryOperator.Subtract)

	const inputMultiplyOperator = () =>
		inputBinaryOperator(BinaryOperator.Multiply)

	const inputDivideOperator = () => inputBinaryOperator(BinaryOperator.Divide)

	function equals(): void {
		if (mode !== CalculatorMode.Start && lastBinaryOperator !== null) {
			const result = doBinaryOperation(
				lastNumber,
				new Dec(currentInputString),
				lastBinaryOperator
			)
			setLastNumber(result)
			setLastBinaryOperator(null)
			setCurrentInputString(String(result))
			setMode(CalculatorMode.Start)
		}
	}

	function handleKeyPress({ key }: KeyboardEvent): void {
		const keyAsInt = parseInt(key, 10)
		if (isDigit(keyAsInt)) {
			inputDigit(keyAsInt)
		} else if (key === '.') {
			inputDecimalPoint()
		} else if (key === '+') {
			inputAddOperator()
		} else if (key === '-') {
			inputSubtractOperator()
		} else if (key === '*') {
			inputMultiplyOperator()
		} else if (key === '/') {
			inputDivideOperator()
		} else if (key === 'c') {
			reset()
		} else if (key === 'b') {
			backspace()
		} else if (key === 'n') {
			negate()
		} else if (key === '=') {
			equals()
		}
	}

	return (
		// eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
		<form className="zbc-calculator" onKeyPress={handleKeyPress}>
			<div className="zbc-calculator__display">
				<label htmlFor={outputId}>Display:</label>
				<output id={outputId} className="zbc-calculator__display__output">
					{display}
				</output>
			</div>
			<div className="zbc-calculator__buttons">
				<div className="zbc-calculator__buttons__numbers">
					{DIGITS.map(digit => (
						<Button key={digit} onClick={() => inputDigit(digit)}>
							{digit}
						</Button>
					))}
					<Button onClick={inputDecimalPoint}>.</Button>
					<Button a11yLabel="Negate" onClick={negate}>
						+/−
					</Button>
				</div>
				<div className="zbc-calculator__buttons__binary-operators">
					<Button a11yLabel="Add" onClick={inputAddOperator}>
						+
					</Button>
					<Button a11yLabel="Subtract" onClick={inputSubtractOperator}>
						−
					</Button>
					<Button a11yLabel="Multiply" onClick={inputMultiplyOperator}>
						×
					</Button>
					<Button a11yLabel="Divide" onClick={inputDivideOperator}>
						∕
					</Button>
				</div>
				<div className="zbc-calculator__buttons__bottom-row">
					<Button onClick={reset} variant="destructive">
						Reset
					</Button>
					<Button
						a11yLabel="Backspace"
						onClick={backspace}
						variant="destructive"
					>
						⌫
					</Button>
					<Button a11yLabel="Equals" onClick={equals} variant="primary">
						=
					</Button>
				</div>
			</div>
		</form>
	)
}
