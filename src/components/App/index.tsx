// External dependencies
import { h, JSX } from 'preact'

// Project component dependencies
import Calculator from '../Calculator'
import Header from '../Header'
import Footer from '../Footer'

// Internal dependencies
import './style.css'

export default function App(): JSX.Element {
	return (
		<main className="zbc">
			<Header />
			<Calculator />
			<Footer />
		</main>
	)
}
