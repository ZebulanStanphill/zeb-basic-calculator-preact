// External dependencies
import classNames from 'classnames'
import { h, JSX, RenderableProps } from 'preact'

// Internal dependencies
import './style.css'

type Variant = 'destructive' | 'primary' | 'regular'

interface Props {
	a11yLabel?: string
	className?: string
	onClick: () => void
	variant?: Variant
}

export default function Button({
	a11yLabel,
	children,
	className,
	onClick,
	variant = 'regular',
}: RenderableProps<Props>): JSX.Element {
	let variantClassName

	switch (variant) {
		case 'destructive':
			variantClassName = 'is-destructive'
			break
		case 'primary':
			variantClassName = 'is-primary'
			break
		default:
			variantClassName = ''
	}

	const classes = classNames('zbc-button', variantClassName, className)
	return (
		<button
			type="button"
			aria-label={a11yLabel}
			title={a11yLabel}
			className={classes}
			onClick={onClick}
		>
			{children}
		</button>
	)
}
