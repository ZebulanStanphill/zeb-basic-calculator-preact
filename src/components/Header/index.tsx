// External dependencies
import { h, JSX } from 'preact'

export default function Header(): JSX.Element {
	return (
		<header className="zbc-header">
			<h1>Zeb’s Basic Calculator (Preact version)</h1>
		</header>
	)
}
